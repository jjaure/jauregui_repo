#include "cube.h"

int
main( int argc, char **argv )
{
  glutInit( &argc, argv );
  glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
  glutInitWindowSize( 700, 700 );
  glutInitWindowPosition( 100, 100 );
  glutCreateWindow( "OpenGL - Creating a Cube" );

  glEnable(GL_DEPTH_TEST);
  glutDisplayFunc( drawCube );
  glutSpecialFunc( keyboard );
  glutMainLoop();

  return 0;
}
