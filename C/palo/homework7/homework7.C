#include <iomanip>
#include <fstream>
#include <iostream> 
#include "homework7.h"

using namespace std;

void readData() {}

int main() {
  // ofstream outputFile , outputFile << "statement" , a way to send data to file.
  //outputFile.open("fileName.txt") ; to open file
  //ifstream inputFile is a way to read data from file.

  vector< vector<string> > studentData;
  ifstream inputFile;
  inputFile.open("gradebook.csv");

  readData(inputFile, studentData);

  inputFile.close();
    
  //for (int count2 = 0; count2 < (count-1); count2++) {
  //  cout << line[count2] << endl;
  //}
  return 0;
}
