#include <sstream>
#include <vector>
#include <string>

void readData(std::istream &input, std::vector<std::vector<std::string> > &output) {
  std::string line;

  while( getline(input, line) ) {
    std::istringstream linestream(line);
    std::string value;
    while( getline(linestream, value, ',') ) {
      std::cout << "Value = " << value << "\n";
    }
  }
}
