#include <thread>
#include <vector>
#include <stdio.h>
// WINH #include <Windows.h>

#include "tetris.h"

int main()
{
  // Create Screen Buffer
  // WIN_Hwchar_t *screen = new wchar_t[nScreenWidth*nScreenHeight];
  
  // WIN_Hfor (int i = 0; i < nScreenWidth*nScreenHeight; i++) 
  // WIN_H  screen[i] = L' ';
  
  // WiN_H HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);

  // WIN_H SetConsoleActiveScreenBuffer(hConsole);
  // WIN_H DWORD dwBytesWritten = 0;
	
  tetromino[0].append(L"..X...X...X...X."); // Tetronimos 4x4
  tetromino[1].append(L"..X..XX...X.....");
  tetromino[2].append(L".....XX..XX.....");
  tetromino[3].append(L"..X..XX..X......");
  tetromino[4].append(L".X...XX...X.....");
  tetromino[5].append(L".X...X...XX.....");
  tetromino[6].append(L"..X...X..XX.....");

  // WIN_HpField = new unsigned char[nFieldWidth*nFieldHeight]; // Create play field buffer
  // WIN_Hfor (int x = 0; x < nFieldWidth; x++) // Board Boundary
    // WIN_Hfor (int y = 0; y < nFieldHeight; y++)
      // WIN_HpField[y*nFieldWidth + x] = (x == 0 || x == nFieldWidth - 1 || y == nFieldHeight - 1) ? 9 : 0;

  // Game Logic
  bool bKey[4];
  int nCurrentPiece = 0;
  int nCurrentRotation = 0;
  // WIN_H int nCurrentX = nFieldWidth / 2;
  int nCurrentY = 0;
  int nSpeed = 20;
  int nSpeedCount = 0;
  bool bForceDown = false;
  bool bRotateHold = true;
  int nPieceCount = 0;
  int nScore = 0;
  vector<int> vLines;
  bool bGameOver = false;

  while (!bGameOver) // Main Loop
  {
    // Timing =======================
    // WiN_H this_thread::sleep_for(50ms);  // Small Step = 1 Game Tick
    nSpeedCount++;
    bForceDown = (nSpeedCount == nSpeed);

    // Input ========================
    // WIN_Hfor (int k = 0; k < 4; k++)  // R   L   D Z
    // WIN_H  bKey[k] = (0x8000 & GetAsyncKeyState((unsigned char)("\x27\x25\x28Z"[k]))) != 0;
		
    // Game Logic ===================

    // Handle player movement
    // WIN_H nCurrentX += (bKey[0] && DoesPieceFit(nCurrentPiece, nCurrentRotation, nCurrentX + 1, nCurrentY)) ? 1 : 0;
    // WIN_H nCurrentX -= (bKey[1] && DoesPieceFit(nCurrentPiece, nCurrentRotation, nCurrentX - 1, nCurrentY)) ? 1 : 0;		
    // WIN_H nCurrentY += (bKey[2] && DoesPieceFit(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1)) ? 1 : 0;

  // Rotate, but latch to stop wild spinning
  if (bKey[3])
  {
    // WIN_H nCurrentRotation += (bRotateHold && DoesPieceFit(nCurrentPiece, nCurrentRotation + 1, nCurrentX, nCurrentY)) ? 1 : 0;
    bRotateHold = false;
  }
  else
    bRotateHold = true;

  // Force the piece down the playfield if it's time
  if (bForceDown)
  {
    // Update difficulty every 50 pieces
    nSpeedCount = 0;
    nPieceCount++;
    if (nPieceCount % 50 == 0)
      if (nSpeed >= 10) 
        nSpeed--;
			
    // Test if piece can be moved down
    // WiN_H if (DoesPieceFit(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY + 1))
    // WiN_H   nCurrentY++; // It can, so do it!
    // WiN_H else {
      // It can't! Lock the piece in place
      // WIN_Hfor (int px = 0; px < 4; px++)
      // WIN_H  for (int py = 0; py < 4; py++)
      // WIN_H    if (tetromino[nCurrentPiece][Rotate(px, py, nCurrentRotation)] != L'.')
      // WIN_H      pField[(nCurrentY + py) * nFieldWidth + (nCurrentX + px)] = nCurrentPiece + 1;

      // Check for lines
      // WIN_H for (int py = 0; py < 4; py++) 
      // WIN_H  if(nCurrentY + py < nFieldHeight - 1) {
      // WiN_H     bool bLine = true;
      // WIN_H    for (int px = 1; px < nFieldWidth - 1; px++)
      // WIN_H      bLine &= (pField[(nCurrentY + py) * nFieldWidth + px]) != 0;

          // WIN_Hif (bLine) {
          // WIN_H  // Remove Line, set to =
          // WIN_H  for (int px = 1; px < nFieldWidth - 1; px++)
          // WIN_H    pField[(nCurrentY + py) * nFieldWidth + px] = 8;
          // WIN_H  vLines.push_back(nCurrentY + py);
          // WIN_H}						
        // WIN_H}

      nScore += 25;

      if(!vLines.empty())  
        nScore += (1 << vLines.size()) * 100;

      // Pick New Piece
      // WIN_H nCurrentX = nFieldWidth / 2;
      nCurrentY = 0;
      nCurrentRotation = 0;
      nCurrentPiece = rand() % 7;

      // If piece does not fit straight away, game over!
      // WiN_H bGameOver = !DoesPieceFit(nCurrentPiece, nCurrentRotation, nCurrentX, nCurrentY);
    // WiN_H }
  }

  // Display ======================

  // Draw Field
  // WIN_H for (int x = 0; x < nFieldWidth; x++)
  // WIN_H   for (int y = 0; y < nFieldHeight; y++)
  // WIN_H     screen[(y + 2)*nScreenWidth + (x + 2)] = L" ABCDEFG=#"[pField[y*nFieldWidth + x]];

  // Draw Current Piece
  // WiN_H for (int px = 0; px < 4; px++)
  // WiN_H   for (int py = 0; py < 4; py++)
  // WiN_H     if (tetromino[nCurrentPiece][Rotate(px, py, nCurrentRotation)] != L'.')
  // WiN_H       screen[(nCurrentY + py + 2)*nScreenWidth + (nCurrentX + px + 2)] = nCurrentPiece + 65;

  // Draw Score
  // WiN_H swprintf_s(&screen[2 * nScreenWidth + nFieldWidth + 6], 16, L"SCORE: %8d", nScore);

  // Animate Line Completion
  if (!vLines.empty()) {
    // Display Frame (cheekily to draw lines)
    // WIN_H WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);
    // WiN_H this_thread::sleep_for(400ms); // Delay a bit

    // WIN_H for (auto &v : vLines)
    // WIN_H   for (int px = 1; px < nFieldWidth - 1; px++) {
    // WIN_H     for (int py = v; py > 0; py--)
    // WIN_H       pField[py * nFieldWidth + px] = pField[(py - 1) * nFieldWidth + px];
    // WIN_H     pField[px] = 0;
    // WIN_H   }

    vLines.clear();
		}
    // Display Frame
    // WIN_H WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);
  }

  // Oh Dear
  // WIN_H CloseHandle(hConsole);
  cout << "Game Over!! Score:" << nScore << endl;
  system("pause");
  return 0;
}
