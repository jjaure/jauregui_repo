#include "life.h"

int main(int c, char **v)
{
  int w = 0, h = 0;
  char *app_name = "gameOfLife.exe";
  
  if ( c < 2 ) {
    printf("Error: Please provide size of grid (W x H)\n");
    printf("  %s 10(w) 10(h)\n", app_name);
    
    return 0;
  }
  else {
    w = atoi(v[1]);
    h = atoi(v[2]);

    game(w, h);
   
    return 0;
  }
}
