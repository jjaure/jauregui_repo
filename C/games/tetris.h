#include <iostream>

using namespace std;

int nScreenWidth = 80;	// Console Screen Size X (columns)
int nScreenHeight = 30;	// Console Screen Size Y (rows)
wstring tetromino[7];
int nFieldWidth = 12;
int nFieldHeight = 18;
unsigned char *pField = nullptr;

int Rotate(int px, int py, int r)
{
  int pi = 0;
  switch (r % 4)
  {
    case 0: // 0 degrees  // 0  1  2  3
      pi = py * 4 + px;  // 4  5  6  7
      break;  // 8  9 10 11
      //12 13 14 15

    case 1: // 90 degrees  //12  8  4  0
      pi = 12 + py - (px * 4);	//13  9  5  1
      break;  //14 10  6  2
      //15 11  7  3

    case 2: // 180 degrees  //15 14 13 12
      pi = 15 - (py * 4) - px;	//11 10  9  8
      break;  // 7  6  5  4
      // 3  2  1  0

    case 3: // 270 degrees  // 3  7 11 15
      pi = 3 - py + (px * 4);  // 2  6 10 14
      break;  // 1  5  9 13
  }  // 0  4  8 12

  return pi;
}

bool DoesPieceFit(int nTetromino, int nRotation, int nPosX, int nPosY)
{
  // All Field cells >0 are occupied
  for (int px = 0; px < 4; px++)
    for (int py = 0; py < 4; py++)
    {
      // Get index into piece
      int pi = Rotate(px, py, nRotation);

      // Get index into field
      int fi = (nPosY + py) * nFieldWidth + (nPosX + px);

      // Check that test is in bounds. Note out of bounds does
      // not necessarily mean a fail, as the long vertical piece
      // can have cells that lie outside the boundary, so we'll
      // just ignore them
      if (nPosX + px >= 0 && nPosX + px < nFieldWidth)
      {
        if (nPosY + py >= 0 && nPosY + py < nFieldHeight)
        {
          // In Bounds so do collision check
          if (tetromino[nTetromino][pi] != L'.' && pField[fi] != 0)
            return false; // fail on first hit
        }
      }
    }
  return true;
}
