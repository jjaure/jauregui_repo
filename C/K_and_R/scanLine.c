#include <stdio.h>

#define OUT 0
#define IN  1

int main(void)
{
  int c, b, br, nw, state;
  int c_cnt, l_cnt, t_cnt, b_cnt;
  nw=br=b=c_cnt=l_cnt=t_cnt=b_cnt=0;
  
  state = OUT;

  while ((c = getchar()) != EOF) {
    ++c_cnt;
    if( c == '\n' ) {
      ++l_cnt;
      state = OUT;
    }
    if( c == '\t' ) {
      ++t_cnt;
      state = OUT;
    }
    if( c == ' ' ) {
      ++b_cnt;
      state = OUT;
      if( (c == ' ') && (b < 1) ) {
        putchar(c);
        b = 1;
      }
      else
        ++br;
    }
    else {
      b = 0;
      putchar(c);
      if( (state == OUT) && (c != '\n') ) {
        state = IN;
        ++nw;
      }
    }
  }

  printf("The value of EOF is: %d\n", EOF);
  printf("The value captured for EOF is %d\n", c); 
  printf("User Input Summary:\n");
  printf("Total characters: %d\n", c_cnt);
  printf("%d lines\n", l_cnt);
  printf("%d words\n", nw);
  printf("%d tabs\n", t_cnt);
  printf("%d blanks (%d removed)\n", b_cnt, br);

  return 0;
}
