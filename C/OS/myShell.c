#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1

int 
main(int argc, char **argv )
{
  char inputCommand[32]  = {0};
  //char *inputCommand;
  char *shell_args[64]; 
  char *tok;
  const char args_sp[128] = " ";
  char *inputEnv[]     = { "PATH=/usr/bin/", 0 };
  int pid_stat, exec_stat, exit_size = 4;
 
  while ( TRUE ) {
    int i = 0;

    printf ("myShell > "); 
    fgets( inputCommand, sizeof(inputCommand), stdin );

    for( int j = 0; inputCommand[j] != '\0'; j++) {
      if( inputCommand[j] == '\n' ) 
        inputCommand[j] = 0;
    }

    tok = strtok(inputCommand, args_sp);
    while ( tok != 0 ) {
      shell_args[i] = tok;
      tok = strtok(0, args_sp);
      i++;
    } 
    if ( !strncmp( "exit", shell_args[0], exit_size ) ) {
      printf ("Stopping Shell.\n");
      exit(EXIT_SUCCESS);
    }
    else {
      if ( fork() != 0 ) 
        waitpid(-1, &pid_stat, 0);
      else
        exec_stat =  execve( shell_args[0], shell_args, inputEnv ) ;
    }
  }
  return 0;
}
