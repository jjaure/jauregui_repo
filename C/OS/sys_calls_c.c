#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int 
main(int argc, char **argv )
{
  char *args[] = { "/usr/bin/ls", "-alrth", 0 };
  char *env[] = { 0 };
  int fork_pid, wait_stat, waitStatus, myPid, myPgrpid;
 
  printf ("Shared System Calls between Linux and MINUX:\n"); 
 
  fork_pid = fork();

  wait_stat = waitpid(-1, &waitStatus,  0); 

  printf ("Waiting...\n");
  
  if( !fork_pid ) { 
    printf ("Child Copy:\n");
    printf ("fork(): pid return = %d\n", fork_pid);
    myPid = getpid();
    printf ("My PID = %d\n", myPid);
    myPgrpid = getpgrp();
    printf ("My Process Group ID = %d\n", myPgrpid);
  }
  else {
    printf ("Parent Copy:\n");
    printf ("fork(): pid return = %d\n", fork_pid);
    myPid = getpid();
    printf ("My PID = %d\n", myPid);
    myPgrpid = getpgrp();
    printf ("My Process Group ID = %d\n", myPgrpid);
  }
  
  //execve(args[0], args, env); 

  return 0;
}
