#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int 
main(int argc, char **argv )
{
  char *newargs[] = { NULL, "Yo", "Yo", "Yo", NULL };
  char *env[]     = { 0 };
  int stat;

  if (argc < 2) {
    printf ("Usage: %s <file to exec>\n", argv[0]); 
    exit (EXIT_FAILURE);
  }
 
  newargs[0] = argv[1];
 
  stat = execve(newargs[0], newargs, env);
  perror("execve");
  printf ("Error Code = %d\n", stat);
  exit(EXIT_FAILURE);
}
