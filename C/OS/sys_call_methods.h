#include <iostream>
#include <iomanip>
#include <stdlib.h>

void type_prompt()
{
  system("clear");
  std::cout << "\n\n\n\n";
  std::cout << "--Command Prompt--\n";
  //std::cout << "JoeShell>>";
} 

void read_command(char command[], char options[])
{
  std::cout << "Command " << "\"" << command << "\"" << "running.\n"; 
  std::cout << "Options: " << options;
}
