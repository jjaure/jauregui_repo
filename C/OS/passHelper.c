#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int 
main(int argc, char **argv )
{
  int SIZE = 20;
  char *pass = "P1n0yM3x11!!\0";
  char *passwords[] = {
                        "MannyOS\0", "M@nnyOS\0", "MannyOS11!!\0", "M@nnyOS11!!\0",
                        "MannyOS1111!!!!\0", "M@nnyOS1111!!!!\0", "myMannyOS11!!\0", 
                        "myM@nnyOS11!!\0", "myMannyOS1111!!!!\0", "myM@nnyOS1111!!!!\0",
                        "ManniOS\0", "M@nniOS\0", "ManniOS11!!\0", "M@nniOS11!!\0",
                        "ManniOS1111!!!!\0", "M@nniOS1111!!!!\0", "myManniOS11!!\0", 
                        "myM@nniOS11!!\0", "myManniOS1111!!!!\0", "myM@nniOS1111!!!!\0"
                       };
  for(int x = 0; x < SIZE; x++)
    printf("%s\n", passwords[x]);
/*
  printf("%s", pass);
*/
  return 0;
}
