#include <iostream>

using std::cout;
using std::ios;

#include <iomanip>
using std::setw;
using std::setiosflags;

#include <cstdlib>
#include <ctime>

void shuffle( int wDeck[][13] )
{
  int row, column;

  for ( int card = 1; card <= 52; card++ ) {
    do {
      row    = rand() % 4;
      column = rand() % 13;
    } while( wDeck[row][column] != 0 );

    wDeck[row][column] = card;

  }
}

