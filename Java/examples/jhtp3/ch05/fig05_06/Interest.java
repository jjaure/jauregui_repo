// Fig. 5.6: Interest.java
// Calculating compound interest
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class Interest {
   public static void main( String args[] )
   {
      double amount, principal = 1000.0, rate = .05;

      DecimalFormat precisionTwo = new DecimalFormat( "0.00" );
      JTextArea outputTextArea = new JTextArea( 11, 20 );

      outputTextArea.append( "Year\tAmount on deposit\n" );
 
      for ( int year = 1; year <= 10; year++ ) {
         amount = principal * Math.pow( 1.0 + rate, year );
         outputTextArea.append( year + "\t" +
            precisionTwo.format( amount ) + "\n" );
      }

      JOptionPane.showMessageDialog(
         null, outputTextArea, "Compound Interest",
         JOptionPane.INFORMATION_MESSAGE );

      System.exit( 0 );  // terminate the application
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
