// Fig. 5.13: BreakLabelTest.java
// Using the break statement with a label
import javax.swing.JOptionPane;

public class BreakLabelTest {
   public static void main( String args[] )
   {
      String output = "";

      stop: {   // labeled compound statement
         for ( int row = 1; row <= 10; row++ ) {
            for ( int column = 1; column <= 5 ; column++ ) {

               if ( row == 5 )
                  break stop; // jump to end of stop block

               output += "*  ";
            }

            output += "\n";
         }

         // the following line is skipped
         output += "\nLoops terminated normally";
      }

      JOptionPane.showMessageDialog(
         null, output,"Testing break with a label",
         JOptionPane.INFORMATION_MESSAGE );
      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
