// Fig. 5.9: DoWhileTest.java
// Using the do/while repetition structure
import java.awt.Graphics;
import javax.swing.JApplet;

public class DoWhileTest extends JApplet {
   public void paint( Graphics g )
   {
      int counter = 1;

      do {
         g.drawOval( 110 - counter * 10, 110 - counter * 10,
                     counter * 20, counter * 20 );
         ++counter;
      } while ( counter <= 10 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
