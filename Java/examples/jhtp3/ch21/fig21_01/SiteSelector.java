// Fig. 21.1: SiteSelector.java
// This program uses a button to load a document from a URL.
import java.net.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.applet.AppletContext;

public class SiteSelector extends JApplet {
   private Hashtable sites;
   private Vector siteNames;

   public void init()
   {
      sites = new Hashtable();
      siteNames = new Vector();

      getSitesFromHTMLParameters();

      Container c = getContentPane();
      c.add( new JLabel( "Choose a site to browse" ),
             BorderLayout.NORTH );

      final JList siteChooser = new JList( siteNames );
      siteChooser.addListSelectionListener(
         new ListSelectionListener() {
            public void valueChanged( ListSelectionEvent e )
            {
               Object o = siteChooser.getSelectedValue();
               URL newDocument = (URL) sites.get( o );
               AppletContext browser = getAppletContext();
               browser.showDocument( newDocument );
            }
         }
      );
      c.add( new JScrollPane( siteChooser ),
             BorderLayout.CENTER );
   }

   private void getSitesFromHTMLParameters()
   {
      // look for applet parameters in the HTML document
      // and add sites to Hashtable
      String title, location;
      URL url;
      int counter = 0;

      while ( true ) {
         title = getParameter( "title" + counter );

         if ( title != null ) {
            location = getParameter( "location" + counter );
            
            try {
               url = new URL( location );
               sites.put( title, url );
               siteNames.addElement( title );
            }
            catch ( MalformedURLException e ) {
               e.printStackTrace();
            }
         }
         else
            break;

         ++counter;  
      }
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
