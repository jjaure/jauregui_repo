// Fig. 21.2: ReadServerFile.java
// This program uses a JEditorPane to display the
// contents of a file on a Web server.
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

public class ReadServerFile extends JFrame {
   private JTextField enter;
   private JEditorPane contents;

   public ReadServerFile()
   {
      super( "Simple Web Browser" );

      Container c = getContentPane();

      enter = new JTextField( "Enter file URL here" );
      enter.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               getThePage( e.getActionCommand() );
            }
         }
      );
      c.add( enter, BorderLayout.NORTH );

      contents = new JEditorPane();
      contents.setEditable( false );
      contents.addHyperlinkListener(
         new HyperlinkListener() {
            public void hyperlinkUpdate( HyperlinkEvent e )
            {
               if ( e.getEventType() ==
                    HyperlinkEvent.EventType.ACTIVATED )
                  getThePage( e.getURL().toString() );
            }
         }
      );
      c.add( new JScrollPane( contents ),
             BorderLayout.CENTER );

      setSize( 400, 300 );
      show();
   }

   private void getThePage( String location )
   {
      setCursor( Cursor.getPredefinedCursor(
                    Cursor.WAIT_CURSOR ) );

      try {
         contents.setPage( location );
         enter.setText( location );
      }
      catch ( IOException io ) {
         JOptionPane.showMessageDialog( this,
            "Error retrieving specified URL",
            "Bad URL",
            JOptionPane.ERROR_MESSAGE );
      }

      setCursor( Cursor.getPredefinedCursor(
                    Cursor.DEFAULT_CURSOR ) );
   }

   public static void main( String args[] )
   {
      ReadServerFile app = new ReadServerFile();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
