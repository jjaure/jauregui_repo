// Fig. 8.8: Employee.java
// Declaration of the Employee class.
package com.deitel.jhtp3.ch08;

public class Employee extends Object {
   private String firstName;
   private String lastName;
   private Date birthDate;
   private Date hireDate;

   public Employee( String fName, String lName,
                    int bMonth, int bDay, int bYear,
                    int hMonth, int hDay, int hYear)
   {
      firstName = fName;
      lastName = lName;
      birthDate = new Date( bMonth, bDay, bYear );
      hireDate = new Date( hMonth, hDay, hYear );
   }

   public String toString()
   {
      return lastName + ", " + firstName +
             "  Hired: " + hireDate.toString() +
             "  Birthday: " + birthDate.toString();
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
