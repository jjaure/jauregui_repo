// Fig. 8.9: PackageDataTest.java
// Classes in the same package (i.e., the same directory)
// can use package access data of other classes in the
// same package.
import javax.swing.JOptionPane;

public class PackageDataTest {
   public static void main( String args[] )
   {
      PackageData d = new PackageData();
      String output;

      output = "After instantiation:\n" + d.toString();

      d.x = 77;          // changing package access data
      d.s = "Good bye";  // changing package access data

      output += "\nAfter changing values:\n" + d.toString();
      JOptionPane.showMessageDialog( null, output,
         "Demonstrating Package Access",
         JOptionPane.INFORMATION_MESSAGE );

      System.exit( 0 );
   }
}

class PackageData {
   int x;     // package access instance variable
   String s;  // package access instance variable

   // constructor
   public PackageData() 
   { 
      x = 0; 
      s = "Hello";
   }               

   public String toString() 
   {
      return "x: " + x + "    s: " + s;
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
