// Fig. 8.11: TimeTest.java
// Chaining method calls together with the this reference
import javax.swing.*;
import com.deitel.jhtp3.ch08.Time4;

public class TimeTest {
   public static void main( String args[] )
   {
      Time4 t = new Time4();
      String output;

      t.setHour( 18 ).setMinute( 30 ).setSecond( 22 );

      output = "Universal time: " + t.toUniversalString() +
               "\nStandard time: " + t.toString() +
               "\n\nNew standard time: " +
               t.setTime( 20, 20, 20 ).toString();

      JOptionPane.showMessageDialog( null, output,
         "Chaining Method Calls",
         JOptionPane.INFORMATION_MESSAGE );

      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
