// Fig. 8.11: Time4.java
// Time4 class definition
package com.deitel.jhtp3.ch08;   // place Time4 in a package
import java.text.DecimalFormat;  // used for number formatting

// This class maintains the time in 24-hour format
public class Time4 extends Object {
   private int hour;     // 0 - 23
   private int minute;   // 0 - 59
   private int second;   // 0 - 59

   // Time4 constructor initializes each instance variable
   // to zero. Ensures that Time object starts in a 
   // consistent state.
   public Time4() { this.setTime( 0, 0, 0 ); }

   // Time4 constructor: hour supplied, minute and second
   // defaulted to 0.
   public Time4( int h ) { this.setTime( h, 0, 0 ); }

   // Time4 constructor: hour and minute supplied, second
   // defaulted to 0.
   public Time4( int h, int m ) { this.setTime( h, m, 0 ); }

   // Time4 constructor: hour, minute and second supplied.
   public Time4( int h, int m, int s )
      { this.setTime( h, m, s ); }

   // Time4 constructor: another Time4 object supplied.
   public Time4( Time4 time )
   {
      this.setTime( time.getHour(),
                    time.getMinute(),
                    time.getSecond() );
   }

   // Set Methods
   // Set a new Time value using military time. Perform 
   // validity checks on the data. Set invalid values to zero.
   public Time4 setTime( int h, int m, int s )
   {
      this.setHour( h );    // set the hour
      this.setMinute( m );  // set the minute
      this.setSecond( s );  // set the second

      return this;     // enables chaining
   }

   // set the hour 
   public Time4 setHour( int h ) 
   { 
      this.hour = ( ( h >= 0 && h < 24 ) ? h : 0 ); 

      return this;     // enables chaining
   }

   // set the minute 
   public Time4 setMinute( int m ) 
   { 
      this.minute = ( ( m >= 0 && m < 60 ) ? m : 0 ); 

      return this;     // enables chaining
   }

   // set the second 
   public Time4 setSecond( int s ) 
   { 
      this.second = ( ( s >= 0 && s < 60 ) ? s : 0 ); 

      return this;     // enables chaining
   }

   // Get Methods
   // get the hour
   public int getHour() { return this.hour; }

   // get the minute
   public int getMinute() { return this.minute; }

   // get the second
   public int getSecond() { return this.second; }

   // Convert to String in universal-time format
   public String toUniversalString()
   {
      DecimalFormat twoDigits = new DecimalFormat( "00" );

      return twoDigits.format( this.getHour() ) + ":" +
             twoDigits.format( this.getMinute() ) + ":" +
             twoDigits.format( this.getSecond() );
   }

   // Convert to String in standard-time format
   public String toString()
   {
      DecimalFormat twoDigits = new DecimalFormat( "00" );

      return ( ( this.getHour() == 12 ||
                 this.getHour() == 0 ) ?
                 12 : this.getHour() % 12 ) + ":" +
             twoDigits.format( this.getMinute() ) + ":" +
             twoDigits.format( this.getSecond() ) +
             ( this.getHour() < 12 ? " AM" : " PM" );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
