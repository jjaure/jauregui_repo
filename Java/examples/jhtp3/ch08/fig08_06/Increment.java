// Fig. 8.6: Increment.java
// Initializing a final variable
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Increment extends JApplet
             implements ActionListener {
   private int count = 0, total = 0;
   private final int INCREMENT = 5; // constant variable
  
   private JButton incr;  

   public void init() 
   { 
      Container c = getContentPane();

      incr = new JButton( "Click to increment" );
      incr.addActionListener( this );
      c.add( incr );
   }

   public void actionPerformed( ActionEvent e )
   {
      total += INCREMENT;
      count++;
      showStatus( "After increment " + count +
                  ": total = " + total );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
