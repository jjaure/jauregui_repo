// Fig. 24.3 : CollectionTest.java
// Using the Collection interface
import java.util.*;
import java.awt.Color;

public class CollectionTest {
   private String colors[] = { "red", "white", "blue" };
                  
   public CollectionTest()
   {
      ArrayList aList = new ArrayList();             

      aList.add( Color.magenta );     // add a color object

      for ( int k = 0; k < colors.length; k++ )
         aList.add( colors[ k ] );         

      aList.add( Color.cyan );        // add a color object

      System.out.println( "\nArrayList: " );
      for ( int k = 0; k < aList.size(); k++ )
         System.out.print( aList.get( k ) + " " );

      removeStrings( aList );

      System.out.println( "\n\nArrayList after calling removeStrings: " );
      for ( int k = 0; k < aList.size(); k++ )
         System.out.print( aList.get( k ) + " " );
   }

   public void removeStrings( Collection c )
   {
      Iterator i = c.iterator();    // get iterator

      while ( i.hasNext() )         // loop while collection has items
        
         if ( i.next() instanceof String )
            i.remove();            // remove String object         
   }

   public static void main( String args[] )
   {
      new CollectionTest();
   }                                           
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
