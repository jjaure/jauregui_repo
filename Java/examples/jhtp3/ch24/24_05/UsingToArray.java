// Fig. 24.5 : UsingToArray.java
// Using method toArray
import java.util.*;

public class UsingToArray {
                     
   public UsingToArray()
   {
      LinkedList links;
      String colors[] = { "black", "blue", "yellow" };

      links = new LinkedList( Arrays.asList( colors ) );      

      links.addLast( "red" );   // add as last item
      links.add( "pink" );      // add to the end
      links.add( 3, "green" );  // add at 3rd index
      links.addFirst( "cyan" ); // add as first item      

      // get the LinkedList elements as an array     
      colors = ( String [] ) links.toArray( new String[ 0 ] );

      System.out.println( "colors: " );
      for ( int k = 0; k < colors.length; k++ )
         System.out.println( colors[ k ] );
   }

   public static void main( String args[] )
   {
      new UsingToArray();
   }                                           
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
