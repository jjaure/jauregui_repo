// Fig. 24.11 : SetTest.java
// Using a HashSet to remove duplicates
import java.util.*;

public class SetTest {
   private String colors[] = { "red", "white", "blue",
                                "green", "gray", "orange",
                                "tan", "white", "cyan",
                                "peach", "gray", "orange" };
                  
   public SetTest()
   {
      ArrayList aList;

      aList = new ArrayList( Arrays.asList( colors ) );
      System.out.println( "ArrayList: " + aList );
      printNonDuplicates( aList );
   }

   public void printNonDuplicates( Collection c )
   {
      HashSet ref = new HashSet( c );   // create a HashSet
      Iterator i = ref.iterator();      // get iterator

      System.out.println( "\nNonduplicates are: " );
      while ( i.hasNext() )
         System.out.print( i.next() + " " );

      System.out.println();
   }                                                    

   public static void main( String args[] )
   {
      new SetTest();
   }                                           
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
