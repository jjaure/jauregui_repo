// Fig. 24.2 : UsingAsList.java
// Using method asList
import java.util.*;

public class UsingAsList {
   private String values[] = { "red", "white", "blue" };
   private List theList;

   public UsingAsList()
   {
      theList = Arrays.asList( values );   // get List
      theList.set( 1, "green" );           // change a value
   }

   public void printElements()
   {
      System.out.print( "List elements : " );
      for ( int k = 0; k < theList.size(); k++ )
         System.out.print( theList.get( k ) + " " );

      System.out.print( "\nArray elements: " );
      for ( int k = 0; k < values.length; k++ )
         System.out.print( values[ k ] + " " );

      System.out.println();
   }

   public static void main( String args[] )
   {
      new UsingAsList().printElements();
   }                                           
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
