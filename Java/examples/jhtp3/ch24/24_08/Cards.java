// Fig. 21.8 : Cards.java
// Using algorithm shuffle
import java.util.*;

class Card {
   private String face;
   private String suit;

   public Card( String face, String suit )
   {
      this.face = face;
      this.suit = suit;
   }

   public String getFace() { return face; }

   public String getSuit() { return suit; }

   public String toString()
   {
      StringBuffer buf = new StringBuffer( face + " of " + suit );

      buf.setLength( 20 );
      return ( buf.toString() );
   }
}

// class Cards definition
public class Cards {
   private static String suits[] = { "Hearts", "Clubs",
                                     "Diamonds", "Spades" };
   private static String faces[] = { "Ace", "Deuce", "Three",
                                     "Four", "Five", "Six",
                                     "Seven", "Eight", "Nine",
                                     "Ten", "Jack", "Queen",
                                     "King" };   
   private List theList;

   public Cards()
   {
      Card deck[] = new Card[ 52 ];

      for ( int k = 0; k < deck.length; k++ )
         deck[ k ] = new Card( faces[ k % 13 ], suits[ k / 13 ] );

      theList = Arrays.asList( deck );   // get List
      Collections.shuffle( theList );    // shuffle deck
   }

   public void printCards()
   {
      int half = theList.size() / 2 - 1;

      for ( int k = 0, k2 = half; k <= half; k++, k2++ )
         System.out.println( theList.get( k ).toString() +
                             theList.get( k2 ) );
   }

   public static void main( String args[] )
   {
      new Cards().printCards();
   }                                           
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
