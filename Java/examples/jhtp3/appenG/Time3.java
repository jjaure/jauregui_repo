// Fig. G.2 Time3.java
// Time3 class definition
package com.deitel.jhtp3.appenG;   // place Time3 in a package
import java.text.DecimalFormat;  // used for number formatting

/**
 * This class maintains the time in 24-hour format.
 * @see java.lang.Object
 * @author Deitel & Associates, Inc.
 */
public class Time3 extends Object {

   private int hour;     // 0 - 23
   private int minute;   // 0 - 59
   private int second;   // 0 - 59

   /**  
    *  Time3 default constructor initializes each 
    *  instance variable to zero. This ensures that Time3 
    *  object starts in a consistent state.
    *  @throws Exception In the case of an invalid time
    */
   public Time3() throws Exception 
      { setTime( 0, 0, 0 ); }

   /** 
    *  Time3 constructor
    *  @param hour the hour
    *  @throws Exception In the case of an invalid time
    */
   public Time3( int h ) throws Exception 
      { setTime( h, 0, 0 ); }

   /** 
    *  Time3 constructor
    *  @param hour the hour
    *  @param minute the minute 
    *  @throws Exception In the case of an invalid time
    */
   public Time3( int h, int m ) throws Exception 
      { setTime( h, m, 0 ); }

    /** 
    *  Time3 constructor
    *  @param hour the hour
    *  @param minute the minute 
    *  @param second the second
    *  @throws Exception In the case of an invalid time
    */
   public Time3( int h, int m, int s ) throws Exception 
      { setTime( h, m, s ); }

   /** 
    *  Time3 constructor
    *  @param time A Time3 object with which to initialize
    *  @throws Exception In the case of an invalid time
    */
   public Time3( Time3 time ) throws Exception
   {
      setTime( time.getHour(),
               time.getMinute(),
               time.getSecond() );
   }

   /**
    *  Set a new time value using universal time. Perform 
    *  validity checks on the data. Set invalid values to zero.
    *  @param h the hour
    *  @param m the minute 
    *  @param s the second
    *  @see com.deitel.jhtp3.appenG.Time3#setHour
    *  @see Time3#setMinute
    *  @see #setSecond
    *  @throws Exception In the case of an invalid time
    */
   public void setTime( int h, int m, int s ) throws Exception
   {
      setHour( h );    // set the hour
      setMinute( m );  // set the minute
      setSecond( s );  // set the second
   }

   /**
    *  Sets the hour.
    *  @param h the hour
    *  @throws Exception In the case of an invalid time
    */
   public void setHour( int h ) throws Exception
   { 
      if( h >= 0 && h < 24 ) 
         hour = h;
      else throw( new Exception() );
   }

   /**
    *  Sets the minute.
    *  @param m the minute
    *  @throws Exception In the case of an invalid time
    */
   public void setMinute( int m ) throws Exception
   {
      if( m >= 0 && m < 60 ) 
         minute = m;
      else throw( new Exception() );
   }

   /**
    *  Sets the second.
    *  @param s the second.
    *  @throws Exception In the case of an invalid time
    */
   public void setSecond( int s ) throws Exception
   { 
      if( s >= 0 && s < 60 ) 
         second = s;
      else throw( new Exception() );
   }

   /**
    *  Gets the hour.
    *  @return an <code>integer</code> specifying the hour.
    */
   public int getHour() { return hour; }

   /**
    *  Gets the minute.
    *  @return an <code>integer</code> specifying the minute.
    */
   public int getMinute() { return minute; }

   /**
    *  Gets the second.
    *  @return an <code>integer</code> specifying the second.
    */
   public int getSecond() { return second; }

   /** Convert to String in universal-time format
    *  @return a <code>String</code> representation 
    *  of the time universal-time format
    */   
   public String toUniversalString()
   {
      DecimalFormat twoDigits = new DecimalFormat( "00" );

      return twoDigits.format( getHour() ) + ":" +
             twoDigits.format( getMinute() ) + ":" +
             twoDigits.format( getSecond() );
   }


   /** Convert to String in standard-time format
    *  @return a <code>String</code> representation 
    *  of the time in standard-time format
    */   
   public String toString()
   {
      DecimalFormat twoDigits = new DecimalFormat( "00" );

      return ( ( getHour() == 12 || getHour() == 0 ) ? 
               12 : getHour() % 12 ) + ":" +
             twoDigits.format( getMinute() ) + ":" +
             twoDigits.format( getSecond() ) +
             ( getHour() < 12 ? " AM" : " PM" );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
