// Fig. 20.3: WeatherInfo.java
// WeatherInfo class definition
import java.rmi.*;
import java.io.Serializable;

public class WeatherInfo implements Serializable {
   private String cityName;
   private String temperature;
   private String description;

   public WeatherInfo( String city, String desc, String temp )
   {
      cityName = city;
      temperature = temp;
      description = desc;
   }

   public String getCityName() { return cityName; }

   public String getTemperature() { return temperature; }

   public String getDescription() { return description; }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
