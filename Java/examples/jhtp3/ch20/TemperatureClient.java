// Fig. 20.4: TemperatureClient.java 
// TemperatureClient definition
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.rmi.*;   

public class TemperatureClient extends JFrame
{
   public TemperatureClient( String ip ) 
   {
      super( "RMI TemperatureClient..." );      
      getRemoteTemp( ip );
  
      setSize( 625, 567 );
      setResizable( false );
      show();
   }

   // obtain weather information from TemperatureServerImpl
   // remote object
   private void getRemoteTemp( String ip )
   {         
      try {
         // name of remote server object bound to rmi registry
         String serverObjectName = "//" + ip + "/TempServer"; 

         // lookup TemperatureServerImpl remote object
         // in rmiregistry
         TemperatureServer mytemp = ( TemperatureServer ) 
            Naming.lookup( serverObjectName );

         // get weather information from server
         WeatherInfo weatherInfo[] = mytemp.getWeatherInfo();
         WeatherItem w[] =
            new WeatherItem[ weatherInfo.length ];
         ImageIcon headerImage =
            new ImageIcon( "images/header.jpg" );

         JPanel p = new JPanel();

         // determine number of rows for the GridLayout;
         // add 3 to accommodate the two header JLabels
         // and balance the columns
         p.setLayout(
            new GridLayout( ( w.length + 3 ) / 2, 2 ) );
         p.add( new JLabel( headerImage ) ); // header 1
         p.add( new JLabel( headerImage ) ); // header 2

         for ( int i = 0; i < w.length; i++ ) {   
            w[ i ] = new WeatherItem( weatherInfo[ i ] );
            p.add( w[ i ] );           
         }

         getContentPane().add( new JScrollPane( p ),
                             BorderLayout.CENTER );
      }
      catch ( java.rmi.ConnectException ce ) {
         System.err.println( "Connection to server failed. " +
            "Server may be temporarily unavailable." );
      }
      catch ( Exception e ) { 
         e.printStackTrace();
         System.exit( 1 ); 
      }      
   }

   public static void main( String args[] )
   {
      TemperatureClient gt = null;

      // if no sever IP address or host name specified,
      // use "localhost"; otherwise use specified host
      if ( args.length == 0 )
         gt = new TemperatureClient( "localhost" );
      else
         gt = new TemperatureClient( args[ 0 ] );

      gt.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
