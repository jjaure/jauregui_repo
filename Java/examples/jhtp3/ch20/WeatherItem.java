// Fig. 20.5: WeatherItem.java
// WeatherItem definition
import java.awt.*;
import javax.swing.*;

public class WeatherItem extends JLabel { 
   private static ImageIcon weatherImages[], backgroundImage;
   private final static String weatherConditions[] =
      { "SUNNY", "PTCLDY", "CLOUDY", "MOCLDY", "TSTRMS",
        "RAIN", "SNOW", "VRYHOT", "FAIR", "RNSNOW",
        "SHWRS", "WINDY", "NOINFO", "MISG" };
   private final static String weatherImageNames[] =
      { "sunny", "pcloudy", "mcloudy", "mcloudy", "rain",
        "rain", "snow", "vryhot", "fair", "rnsnow",
        "showers", "windy", "noinfo", "noinfo" };

   // static initializer block to load weather images
   static {
      backgroundImage = new ImageIcon( "images/back.jpg" );
      weatherImages =
         new ImageIcon[ weatherImageNames.length ];

      for ( int i = 0; i < weatherImageNames.length; ++i )
         weatherImages[ i ] = new ImageIcon( 
            "images/" + weatherImageNames[ i ] + ".jpg" );
   }
  
   // instance variables
   private ImageIcon weather;
   private WeatherInfo weatherInfo;

   public WeatherItem( WeatherInfo w )
   {
      weather = null;
      weatherInfo = w;

      // locate image for city's weather condition
      for ( int i = 0; i < weatherConditions.length; ++i )  
         if ( weatherConditions[ i ].equals(
              weatherInfo.getDescription().trim() ) ) {
            weather = weatherImages[ i ];
            break;
         }

      // pick the "no info" image if either there is no
      // weather info or no image for the current
      // weather condition
      if ( weather == null ) {  
         weather = weatherImages[ weatherImages.length - 1 ];
         System.err.println( "No info for: " +
                             weatherInfo.getDescription() );
      }
   }

   public void paintComponent( Graphics g )
   {
      super.paintComponent( g );
      backgroundImage.paintIcon( this, g, 0, 0 );

      Font f = new Font( "SansSerif", Font.BOLD, 12 );
      g.setFont( f );
      g.setColor( Color.white );
      g.drawString( weatherInfo.getCityName(), 10, 19 );
      g.drawString( weatherInfo.getTemperature(), 130, 19 );

      weather.paintIcon( this, g, 253, 1 );
   }

   // make WeatherItem's preferred size the width and height of
   // the background image
   public Dimension getPreferredSize()
   {
      return new Dimension( backgroundImage.getIconWidth(),
                            backgroundImage.getIconHeight() );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
