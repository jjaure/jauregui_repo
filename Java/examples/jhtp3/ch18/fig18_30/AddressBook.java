// Fig. 18.30: AddressBook.java
// Inserting into, updating and searching through a database
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AddressBook extends JFrame {
   private ControlPanel controls;
   private ScrollingPanel scrollArea;
   private JTextArea output;
   private String url;
   private Connection connect;
   private JScrollPane textpane;

   public AddressBook()
   {
      super( "Address Book Database Application" );

      Container c = getContentPane();

      // Start screen layout
      scrollArea = new ScrollingPanel();
      output = new JTextArea( 6, 30 );
      c.setLayout( new BorderLayout() );
      c.add( new JScrollPane( scrollArea ),
             BorderLayout.CENTER );
      textpane = new JScrollPane( output );
      c.add( textpane, BorderLayout.SOUTH );

      // Set up database connection
      try {
         url = "jdbc:odbc:AddressBook";

         Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
         connect = DriverManager.getConnection( url );
         output.append( "Connection successful\n" );
      }
      catch ( ClassNotFoundException cnfex ) {
         // process ClassNotFoundExceptions here
         cnfex.printStackTrace();
         output.append( "Connection unsuccessful\n" +
                        cnfex.toString() );
      }
      catch ( SQLException sqlex ) {
         // process SQLExceptions here
         sqlex.printStackTrace();
         output.append( "Connection unsuccessful\n" +
                        sqlex.toString() );
      }
      catch ( Exception ex ) {
         // process remaining Exceptions here
         ex.printStackTrace();
         output.append( ex.toString() );
      }

      // Complete screen layout
      controls =
         new ControlPanel( connect, scrollArea, output);
      c.add( controls, BorderLayout.NORTH );

      setSize( 500, 500 );
      show();
   }

   public static void main( String args[] )
   {
      AddressBook app = new AddressBook();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
