// Fig. 18.30: ControlPanel.java
// Class ControlPanel definition
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;

public class ControlPanel extends JPanel {
   private JButton findName, addName,
                   updateName, clear, help;
   
   public ControlPanel( Connection c, ScrollingPanel s, 
                        JTextArea t )
   {
      setLayout( new GridLayout( 1, 5 ) );
      
      findName = new JButton( "Find" );
      findName.addActionListener( new FindRecord( c, s, t ) );
      add( findName );

      addName = new JButton( "Add" );
      addName.addActionListener( new AddRecord( c, s, t ) );
      add( addName );
      
      updateName = new JButton( "Update" );
      updateName.addActionListener(
         new UpdateRecord( c, s, t ) );
      add( updateName );
      
      clear = new JButton( "Clear" );
      clear.addActionListener( new ClearFields( s ) );
      add( clear );
      
      help = new JButton( "Help" );
      help.addActionListener( new Help( t ) );
      add( help );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
