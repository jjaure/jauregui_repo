// Fig. 18.30: Help.java
// Class Help definition
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Help implements ActionListener {
   private JTextArea output;

   public Help( JTextArea o )
   {
      output = o;
   }

   public void actionPerformed( ActionEvent e )
   {
      output.append( "\nClick Find to locate a record.\n" +
                     "Click Add to insert a new record.\n" +
                     "Click Update to update " +
                     "the information in a record.\n" +
                     "Click Clear to empty" +
                     " the textfields.\n" );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
