// Fig. 18.30: ClearFields.java
// Class ClearFields definition
import java.awt.*;
import java.awt.event.*;

public class ClearFields implements ActionListener {
   private ScrollingPanel fields;

   public ClearFields( ScrollingPanel f )
   {
      fields = f;
   }

   public void actionPerformed( ActionEvent e )
   {
      fields.id.setText( "" );
      fields.first.setText( "" );
      fields.last.setText( "" );
      fields.address.setText( "" );
      fields.city.setText( "" );
      fields.state.setText( "" );
      fields.zip.setText( "" );
      fields.country.setText( "" );
      fields.email.setText( "" );
      fields.home.setText( "" );
      fields.fax.setText( "" );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
