// Fig. 17.10: Record.java
// Record class for the RandomAccessFile programs.
package com.deitel.jhtp3.ch17;
import java.io.*;
import com.deitel.jhtp3.ch17.BankAccountRecord;

public class Record extends BankAccountRecord {
  
   public Record()
   {
      this( 0, "", "", 0.0 );
   }

   public Record( int acct, String first,
                  String last, double bal )
   {
      super( acct, first, last, bal );
   }

   // Read a record from the specified RandomAccessFile
   public void read( RandomAccessFile file ) throws IOException
   {
      setAccount( file.readInt() );   
      setFirstName( padName( file ) );
      setLastName( padName( file ) );
      setBalance( file.readDouble() );
   }

   private String padName( RandomAccessFile f )
      throws IOException
   {
      char name[] = new char[ 15 ], temp;

      for ( int i = 0; i < name.length; i++ ) {
         temp = f.readChar();
         name[ i ] = temp;
      }     
      
      return new String( name ).replace( '\0', ' ' );
   }

   // Write a record to the specified RandomAccessFile
   public void write( RandomAccessFile file ) throws IOException
   {
      file.writeInt( getAccount() );
      writeName( file, getFirstName() );
      writeName( file, getLastName() );
      file.writeDouble( getBalance() );
   }

   private void writeName( RandomAccessFile f, String name )
      throws IOException
   {
      StringBuffer buf = null;

      if ( name != null ) 
         buf = new StringBuffer( name );
      else 
         buf = new StringBuffer( 15 );

      buf.setLength( 15 );
      f.writeChars( buf.toString() );
   }

   // NOTE: This method contains a hard coded value for the
   // size of a record of information.
   public static int size() { return 72; }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
