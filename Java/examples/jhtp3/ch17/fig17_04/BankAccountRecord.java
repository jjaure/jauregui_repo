// Fig. 17.4: BankAccountRecord.java
// A class that represents one record of information.
package com.deitel.jhtp3.ch17;
import java.io.Serializable;

public class BankAccountRecord implements Serializable {
   private int account;
   private String firstName;
   private String lastName;
   private double balance;
   
   public BankAccountRecord() 
   {
      this( 0, "", "", 0.0 );
   }
  
   public BankAccountRecord( int acct, String first,
                             String last, double bal )
   {
      setAccount( acct );
      setFirstName( first );
      setLastName( last );
      setBalance( bal );
   }
   
   public void setAccount( int acct )
   {
      account = acct;
   }

   public int getAccount() { return account; }
   
   public void setFirstName( String first )
   {
      firstName = first;
   }

   public String getFirstName() { return firstName; }
   
   public void setLastName( String last )
   {
      lastName = last;
   }

   public String getLastName() { return lastName; }
   
   public void setBalance( double bal )
   {
      balance = bal;
   }

   public double getBalance() { return balance; }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
