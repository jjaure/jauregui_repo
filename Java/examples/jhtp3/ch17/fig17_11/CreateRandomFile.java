// Fig. 17.11: CreateRandFile.java
// This program creates a random access file sequentially
// by writing 100 empty records to disk.
import com.deitel.jhtp3.ch17.Record;
import java.io.*;
import javax.swing.*;

public class CreateRandomFile {
   private Record blank;
   private RandomAccessFile file;

   public CreateRandomFile()
   {
      blank = new Record();
      openFile();
   }

   private void openFile()
   {
      JFileChooser fileChooser = new JFileChooser();
                   fileChooser.setFileSelectionMode(
                              JFileChooser.FILES_ONLY );
      int result = fileChooser.showSaveDialog( null );
   
      // user clicked Cancel button on dialog
      if ( result == JFileChooser.CANCEL_OPTION )
         return;

      File fileName = fileChooser.getSelectedFile();

      if ( fileName == null || 
           fileName.getName().equals( "" ) )
         JOptionPane.showMessageDialog( null,
            "Invalid File Name",
            "Invalid File Name",
            JOptionPane.ERROR_MESSAGE );
      else {
         // Open the file
         try {           
            file = new RandomAccessFile( fileName, "rw" );

            for ( int i = 0; i < 100; i++ )
               blank.write( file );

            System.exit( 0 );
         }
         catch ( IOException e ) {
            JOptionPane.showMessageDialog( null,
               "File does not exist",
               "Invalid File Name",
               JOptionPane.ERROR_MESSAGE );
            System.exit( 1 );
         }
      }
   }

   public static void main( String args[] )
   {
      new CreateRandomFile();
   }   
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
