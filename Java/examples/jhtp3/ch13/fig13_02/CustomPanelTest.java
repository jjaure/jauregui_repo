// Fig. 13.2: CustomPanelTest.java
// Using a customized Panel object.
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CustomPanelTest extends JFrame {
   private JPanel buttonPanel;
   private CustomPanel myPanel;
   private JButton circle, square;

   public CustomPanelTest()
   {
      super( "CustomPanel Test" );

      myPanel = new CustomPanel();   // instantiate canvas
      myPanel.setBackground( Color.green );

      square = new JButton( "Square" );
      square.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               myPanel.draw( CustomPanel.SQUARE );
            }
         }
      );

      circle = new JButton( "Circle" );
      circle.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               myPanel.draw( CustomPanel.CIRCLE );
            }
         }
      );

      buttonPanel = new JPanel();
      buttonPanel.setLayout( new GridLayout( 1, 2 ) );
      buttonPanel.add( circle );
      buttonPanel.add( square );

      Container c = getContentPane();
      c.add( myPanel, BorderLayout.CENTER );  
      c.add( buttonPanel, BorderLayout.SOUTH );

      setSize( 300, 150 );
      show();
   }

   public static void main( String args[] )
   {
      CustomPanelTest app = new CustomPanelTest();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
