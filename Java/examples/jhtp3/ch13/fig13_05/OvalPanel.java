// Fig. 13.5: OvalPanel.java
// A customized JPanel class.
import java.awt.*;
import javax.swing.*;

public class OvalPanel extends JPanel {
   private int diameter = 10;

   public void paintComponent( Graphics g )
   {
      super.paintComponent( g );
      g.fillOval( 10, 10, diameter, diameter );
   }

   public void setDiameter( int d )
   {
      diameter = ( d >= 0 ? d : 10 );  // default diameter 10
      repaint();
   }

   // the following methods are used by layout managers
   public Dimension getPreferredSize()
   {
      return new Dimension( 200, 200 );
   }

   public Dimension getMinimumSize()
   {
      return getPreferredSize();
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
