// Fig. 13.5: SliderDemo.java
// Using JSliders to size an oval.
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class SliderDemo extends JFrame {
   private JSlider diameter;
   private OvalPanel myPanel;

   public SliderDemo() 
   {
      super( "Slider Demo" );

      myPanel = new OvalPanel();
      myPanel.setBackground( Color.yellow );

      diameter = new JSlider( SwingConstants.HORIZONTAL,
                              0, 200, 10 );
      diameter.setMajorTickSpacing( 10 );
      diameter.setPaintTicks( true );
      diameter.addChangeListener(
         new ChangeListener() {
            public void stateChanged( ChangeEvent e )
            {
               myPanel.setDiameter( diameter.getValue() );
            }
         }
      );

      Container c = getContentPane();
      c.add( diameter, BorderLayout.SOUTH );
      c.add( myPanel, BorderLayout.CENTER );

      setSize( 220, 270 );
      show();
   }

   public static void main( String args[] )
   {
      SliderDemo app = new SliderDemo();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
