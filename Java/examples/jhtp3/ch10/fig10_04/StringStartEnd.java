// Fig. 10.4: StringStartEnd.java
// This program demonstrates the methods startsWith and
// endsWith of the String class.
import javax.swing.*;

public class StringStartEnd {
   public static void main( String args[] )
   {
      String strings[] = { "started", "starting", 
                           "ended", "ending" };
      String output = "";

      // Test method startsWith
      for ( int i = 0; i < strings.length; i++ )
         if ( strings[ i ].startsWith( "st" ) )
            output += "\"" + strings[ i ] +
                      "\" starts with \"st\"\n";

      output += "\n";

      // Test method startsWith starting from position
      // 2 of the string
      for ( int i = 0; i < strings.length; i++ )
         if ( strings[ i ].startsWith( "art", 2 ) ) 
            output += 
               "\"" + strings[ i ] +
               "\" starts with \"art\" at position 2\n";

      output += "\n";

      // Test method endsWith
      for ( int i = 0; i < strings.length; i++ )
         if ( strings[ i ].endsWith( "ed" ) )
            output += "\"" + strings[ i ] +
                      "\" ends with \"ed\"\n";

      JOptionPane.showMessageDialog( null, output,
         "Demonstrating String Class Comparisons",
         JOptionPane.INFORMATION_MESSAGE );

      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
