// Fig. 10.18: StaticCharMethods2.java
// Demonstrates the static character conversion methods
// of class Character from the java.lang package.
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StaticCharMethods2 extends JFrame {
   private char c;
   private int digit, radix;
   private JLabel prompt1, prompt2;
   private JTextField input, radixField;
   private JButton toChar, toInt;

   public StaticCharMethods2()
   {
      super( "Character Conversion Methods" );

      Container con = getContentPane();
      con.setLayout( new FlowLayout() );

      prompt1 = new JLabel( "Enter a digit or character " );
      input = new JTextField( 5 );
      con.add( prompt1 );
      con.add( input );

      prompt2 = new JLabel( "Enter a radix " );
      radixField = new JTextField( 5 );
      con.add( prompt2 );
      con.add( radixField );

      toChar = new JButton( "Convert digit to character" );
      toChar.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {  
               digit = Integer.parseInt( input.getText() );
               radix =
                  Integer.parseInt( radixField.getText() );
               JOptionPane.showMessageDialog( null,
                  "Convert digit to character: " +
                  Character.forDigit( digit, radix ) );
            }
         }
      );
      con.add( toChar );

      toInt = new JButton( "Convert character to digit" );
      toInt.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {  
               String s = input.getText();
               c = s.charAt( 0 );
               radix =
                  Integer.parseInt( radixField.getText() );
               JOptionPane.showMessageDialog( null,
                  "Convert character to digit: " +
                  Character.digit( c, radix ) );
            }
         }
      );
      con.add( toInt );

      setSize( 275, 150 );  // set the window size
      show();               // show the window
   }

   public static void main( String args[] )
   {
      StaticCharMethods2 app = new StaticCharMethods2();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
