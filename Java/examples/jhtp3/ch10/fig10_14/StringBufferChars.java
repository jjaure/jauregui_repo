// Fig. 10.14: StringBufferChars.java
// The charAt, setCharAt, getChars, and reverse methods 
// of class StringBuffer.
import javax.swing.*;

public class StringBufferChars {
   public static void main( String args[] )
   {
      StringBuffer buf = new StringBuffer( "hello there" );

      String output = "buf = " + buf.toString() +
                      "\nCharacter at 0: " + buf.charAt( 0 ) +
                      "\nCharacter at 4: " + buf.charAt( 4 );

      char charArray[] = new char[ buf.length() ];
      buf.getChars( 0, buf.length(), charArray, 0 );
      output += "\n\nThe characters are: ";

      for ( int i = 0; i < charArray.length; ++i )
         output += charArray[ i ];

      buf.setCharAt( 0, 'H' );
      buf.setCharAt( 6, 'T' );
      output += "\n\nbuf = " + buf.toString();

      buf.reverse();
      output += "\n\nbuf = " + buf.toString();

      JOptionPane.showMessageDialog( null, output,
         "Demonstrating StringBuffer Character Methods",
         JOptionPane.INFORMATION_MESSAGE );

      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
