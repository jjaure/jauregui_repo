// Fig. 15.6: SharedCell.java
// Show multiple threads modifying shared object.
import java.text.DecimalFormat;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SharedCell extends JFrame {
   public SharedCell()
   {
      super( "Demonstrating Thread Synchronization" );
      JTextArea output = new JTextArea( 20, 30 );

      getContentPane().add( new JScrollPane( output ) );
      setSize( 500, 500 );
      show();

      // set up threads and start threads
      HoldIntegerSynchronized h =
         new HoldIntegerSynchronized( output );
      ProduceInteger p = new ProduceInteger( h, output );
      ConsumeInteger c = new ConsumeInteger( h, output );

      p.start();
      c.start();
   }

   public static void main( String args[] )
   {
      SharedCell app = new SharedCell();
      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
