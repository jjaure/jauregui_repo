// Fig. 15.6: HoldIntegerSynchronized.java
// Definition of class HoldIntegerSynchronized that
// uses thread synchronization to ensure that both
// threads access sharedInt at the proper times.
import javax.swing.JTextArea;
import java.text.DecimalFormat;

public class HoldIntegerSynchronized {
   private int sharedInt[] = { -1, -1, -1, -1, -1 };
   private boolean writeable = true;
   private boolean readable = false;
   private int readLoc = 0, writeLoc = 0;
   private JTextArea output;

   public HoldIntegerSynchronized( JTextArea o )
   {
      output = o;
   }

   public synchronized void setSharedInt( int val )
   {
      while ( !writeable ) {
         try {
            output.append( " WAITING TO PRODUCE " + val );
            wait();
         }
         catch ( InterruptedException e ) {
            System.err.println( e.toString() );
         }
      }

      sharedInt[ writeLoc ] = val;
      readable = true;

      output.append( "\nProduced " + val +
                     " into cell " + writeLoc );

      writeLoc = ( writeLoc + 1 ) % 5;

      output.append( "\twrite " + writeLoc +
                     "\tread " + readLoc);
      displayBuffer( output, sharedInt );

      if ( writeLoc == readLoc ) {
         writeable = false;
         output.append( "\nBUFFER FULL" );
      }

      notify();
   }

   public synchronized int getSharedInt()
   {
      int val;

      while ( !readable ) {
         try {
            output.append( " WAITING TO CONSUME" );
            wait();
         }
         catch ( InterruptedException e ) {
            System.err.println( e.toString() );
         }
      }

      writeable = true;
      val = sharedInt[ readLoc ];

      output.append( "\nConsumed " + val +
                     " from cell " + readLoc );

      readLoc = ( readLoc + 1 ) % 5;

      output.append( "\twrite " + writeLoc +
                     "\tread " + readLoc );
      displayBuffer( output, sharedInt );

      if ( readLoc == writeLoc ) {
         readable = false;
         output.append( "\nBUFFER EMPTY" );
      }

      notify();
      return val;
   }

   public void displayBuffer( JTextArea out, int buf[] )
   {
      DecimalFormat formatNumber = new DecimalFormat( " #;-#" );
      output.append( "\tbuffer: " );

      for ( int i = 0; i < buf.length; i++ )
         out.append( " " + formatNumber.format( buf[ i ] ));
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
