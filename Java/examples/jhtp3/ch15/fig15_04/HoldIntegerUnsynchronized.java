// Fig. 15.4: HoldIntegerUnsynchronized.java
// Definition of class HoldIntegerUnsynchronized
public class HoldIntegerUnsynchronized {
   private int sharedInt = -1;

   public void setSharedInt( int val )
   {
      System.err.println( Thread.currentThread().getName() +
         " setting sharedInt to " + val );
      sharedInt = val;
   }

   public int getSharedInt()
   {
      System.err.println( Thread.currentThread().getName() +
         " retrieving sharedInt value " + sharedInt );
      return sharedInt;
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
