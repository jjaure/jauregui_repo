// Fig. 22.11: StackComposition.java
// Class StackComposition definition with composed List object
package com.deitel.jhtp3.ch22;

public class StackComposition {
   private List s;

   public StackComposition() { s = new List( "stack" ); }
   public void push( Object o )
      { s.insertAtFront( o ); }
   public Object pop() throws EmptyListException
      { return s.removeFromFront(); }
   public boolean isEmpty() { return s.isEmpty(); }
   public void print() { s.print(); }
}


/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
