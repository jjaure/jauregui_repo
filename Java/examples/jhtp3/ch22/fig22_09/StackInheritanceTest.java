// Fig. 22.9: StackInheritanceTest.java
// Class StackInheritanceTest
import com.deitel.jhtp3.ch22.StackInheritance;
import com.deitel.jhtp3.ch22.EmptyListException;

public class StackInheritanceTest {
   public static void main( String args[] )
   {
      StackInheritance objStack = new StackInheritance();  

      // Create objects to store in the stack
      Boolean b = Boolean.TRUE;
      Character c = new Character( '$' );
      Integer i = new Integer( 34567 );
      String s = "hello";

      // Use the push method
      objStack.push( b );
      objStack.print();
      objStack.push( c );
      objStack.print();
      objStack.push( i );
      objStack.print();
      objStack.push( s );
      objStack.print();

      // Use the pop method
      Object removedObj = null;

      try {
         while ( true ) {
            removedObj = objStack.pop();
            System.out.println( removedObj.toString() +
                                " popped" );
            objStack.print();
         }
      }
      catch ( EmptyListException e ) {
         System.err.println( "\n" + e.toString() );
      }
   }
}


/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
