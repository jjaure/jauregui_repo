// Fig. 7.10: PassArray.java
// Passing arrays and individual array elements to methods
import java.awt.Container;
import javax.swing.*;

public class PassArray extends JApplet {
   JTextArea outputArea;
   String output;

   public void init()
   {
      outputArea = new JTextArea();
      Container c = getContentPane();
      c.add( outputArea );

      int a[] = { 1, 2, 3, 4, 5 };
      
      output = "Effects of passing entire " +
               "array call-by-reference:\n" +
               "The values of the original array are:\n";

      for ( int i = 0; i < a.length; i++ )
         output += "   " + a[ i ];
   
      modifyArray( a );  // array a passed call-by-reference
   
      output += "\n\nThe values of the modified array are:\n";

      for ( int i = 0; i < a.length; i++ ) 
         output += "   " + a[ i ];
   
      output += "\n\nEffects of passing array " +
                "element call-by-value:\n" +
                "a[3] before modifyElement: " + a[ 3 ];
   
      modifyElement( a[ 3 ] );
   
      output += "\na[3] after modifyElement: " + a[ 3 ];
      outputArea.setText( output );
   }
   
   public void modifyArray( int b[] )
   {
      for ( int j = 0; j < b.length; j++ )
         b[ j ] *= 2;
   }
   
   public void modifyElement( int e )
   {
      e *= 2;
   }   
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
