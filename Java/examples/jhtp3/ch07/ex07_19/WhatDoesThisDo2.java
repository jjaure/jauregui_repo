// Exercise 7.19: WhatDoesThisDo2.java
import java.awt.*;
import javax.swing.*;

public class WhatDoesThisDo2 extends JApplet {
   public void init()
   {
      int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
      JTextArea outputArea = new JTextArea();

      someFunction( a, 0, outputArea );

      Container c = getContentPane();
      c.add( outputArea );
   }
   
   public void someFunction( int b[], int x, JTextArea out )
   {
      if ( x < b.length ) {
         someFunction( b, x + 1, out );
         out.append( b[ x ] + "  " );
      }
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
