// Exercise 6.6: SphereTest.java
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SphereTest extends JApplet
             implements ActionListener {
   JLabel prompt;
   JTextField input;

   public void init()
   {
      Container c = getContentPane();
      c.setLayout( new FlowLayout() );

      prompt = new JLabel( "Enter sphere radius: " );
      input = new JTextField( 10 );
      input.addActionListener( this );
      c.add( prompt );
      c.add( input );
   }

   public void actionPerformed( ActionEvent e )
   {
      double radius =
         Double.parseDouble( e.getActionCommand() );
      showStatus( "Volume is " + sphereVolume( radius ) );
   }

   public double sphereVolume( double radius )
   {
      double volume =
         ( 4.0 / 3.0 ) * Math.PI * Math.pow( radius, 3 );

      return volume;
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
