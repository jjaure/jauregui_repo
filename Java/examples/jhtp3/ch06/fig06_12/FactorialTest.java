// Fig. 6.12: FactorialTest.java
// Recursive factorial method
import java.awt.*;
import javax.swing.*;

public class FactorialTest extends JApplet {
   JTextArea outputArea;

   public void init()
   {
      outputArea = new JTextArea();

      Container c = getContentPane();
      c.add( outputArea );

      // calculate the factorials of 0 through 10
      for ( long i = 0; i <= 10; i++ )
         outputArea.append(
            i + "! = " + factorial( i ) + "\n" );
   }
   
   // Recursive definition of method factorial
   public long factorial( long number )
   {                  
      if ( number <= 1 )  // base case
         return 1;
      else
         return number * factorial( number - 1 );
   }  
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
