// Fig. 25.33: SliderFieldPanel.java
// A subclass of JPanel containing a JSlider and a JTextField
package jhtp3beans;

import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;

public class SliderFieldPanel extends JPanel 
                              implements Serializable {
   private JSlider slider;
   private JTextField field;
   private Box boxContainer;
   private int currentValue;
   
   // object to support bound property changes
   private PropertyChangeSupport changeSupport;

   public SliderFieldPanel()
   {
      // create PropertyChangeSupport for bound properties
      changeSupport = new PropertyChangeSupport( this );

      slider = 
         new JSlider( SwingConstants.HORIZONTAL, 1, 100, 1 );
      field = new JTextField( 
                 String.valueOf( slider.getValue() ), 5 );

      boxContainer = new Box( BoxLayout.X_AXIS );
      boxContainer.add( slider );
      boxContainer.add( Box.createHorizontalStrut( 5 ) );
      boxContainer.add( field );

      setLayout( new BorderLayout() );
      add( boxContainer );

      slider.addChangeListener( 
         new ChangeListener() {
            public void stateChanged( ChangeEvent e )
            {
               setCurrentValue( slider.getValue() );
            }
         }
      );      

      field.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               setCurrentValue(
                  Integer.parseInt( field.getText() ) );
            }
         }
      );      
   }

   // methods for adding and removing PropertyChangeListeners
   public void addPropertyChangeListener( 
      PropertyChangeListener listener ) 
   {
      changeSupport.addPropertyChangeListener( listener );
   }

   public void removePropertyChangeListener( 
      PropertyChangeListener listener ) 
   {
      changeSupport.removePropertyChangeListener( listener );
   }

   // property minimumValue
   public void setMinimumValue( int min ) 
   { 
      slider.setMinimum( min ); 
      
      if ( slider.getValue() < slider.getMinimum() ) {         
         slider.setValue( slider.getMinimum() );
         field.setText( String.valueOf( slider.getValue() ) );
      }
   }

   public int getMinimumValue() 
   { 
      return slider.getMinimum(); 
   }

   // property maximumValue
   public void setMaximumValue( int max )
   {
      slider.setMaximum( max ); 
      
      if ( slider.getValue() > slider.getMaximum() ) {
         slider.setValue( slider.getMaximum() );
         field.setText( String.valueOf( slider.getValue() ) );
      }
   }

   public int getMaximumValue() 
   { 
      return slider.getMaximum(); 
   }

   // property currentValue
   public void setCurrentValue( int current )
   { 
      int oldValue = currentValue;
      currentValue = current;
      slider.setValue( currentValue );
      field.setText( String.valueOf( currentValue ) );
      changeSupport.firePropertyChange( 
         "currentValue", new Integer( oldValue ), 
         new Integer( currentValue ) );
   }   

   public int getCurrentValue() 
   {
      return slider.getValue(); 
   }

   // property fieldWidth
   public void setFieldWidth( int cols ) 
   { 
      field.setColumns( cols );
      boxContainer.validate();
   }

   public int getFieldWidth() 
   { 
      return field.getColumns(); 
   }

   public Dimension getMinimumSize()
   {
      return boxContainer.getMinimumSize();
   }

   public Dimension getPreferredSize()
   {
      return boxContainer.getPreferredSize();
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
