// Fig. 25.38: SliderFieldPanelBeanInfo.java
// The BeanInfo class for SliderFieldPanel
package jhtp3beans;

import java.beans.*;

public class SliderFieldPanelBeanInfo extends SimpleBeanInfo {
   public final static Class beanClass =
      SliderFieldPanel.class;

   public PropertyDescriptor[] getPropertyDescriptors()
   {
      try {
         PropertyDescriptor fieldWidth =
            new PropertyDescriptor( "fieldWidth", beanClass );
         PropertyDescriptor currentValue =
            new PropertyDescriptor(
               "currentValue", beanClass );
         PropertyDescriptor maximumValue =
            new PropertyDescriptor(
               "maximumValue", beanClass );
         PropertyDescriptor minimumValue =
            new PropertyDescriptor( "minimumValue", beanClass );

         // ensure PropertyChangeEvent occurs for this property
         currentValue.setBound( true );

         PropertyDescriptor descriptors[] = { fieldWidth,
            currentValue, maximumValue, minimumValue };

         return descriptors;
      }
      catch ( IntrospectionException ie ) {
         throw new RuntimeException( ie.toString() );
      }
   }

   // the index for the currentValue property
   public int getDefaultPropertyIndex()
   {
      return 1;
   }

   public EventSetDescriptor[] getEventSetDescriptors() {
      try {
         EventSetDescriptor changed = 
            new EventSetDescriptor( beanClass,
            "propertyChange", 
            java.beans.PropertyChangeListener.class,
            "propertyChange");

         changed.setDisplayName( 
            "SliderFieldPanel value changed" );
	
         EventSetDescriptor[] descriptors = { changed };

         return descriptors;
      } 
      catch (IntrospectionException e) {
         throw new RuntimeException(e.toString());
      }
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
