// Fig. 25.30: LogoAnimator2.java
// Animation bean with animationDelay property
package jhtp3beans;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LogoAnimator2 extends LogoAnimator {
   // the following two methods are
   // for the animationDelay property
   public void setAnimationDelay( int value )
   {
      animationDelay = value;
      animationTimer.setDelay( animationDelay );
   }

   public int getAnimationDelay()
   {
      return animationTimer.getDelay();
   }

   public static void main( String args[] )
   {
      LogoAnimator2 anim = new LogoAnimator2();

      JFrame app = new JFrame( "Animator test" );
      app.getContentPane().add( anim, BorderLayout.CENTER );

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );

      app.setSize( anim.getPreferredSize().width + 10,
                 anim.getPreferredSize().height + 30 );
      app.show();
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
