// Fig. 23.1: VectorTest.java
// Testing the Vector class of the java.util package
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class VectorTest extends JFrame {

   public VectorTest()
   {
      super( "Vector Example" );

      final JLabel status = new JLabel();
      Container c = getContentPane();
      final Vector v = new Vector( 1 );            

      c.setLayout( new FlowLayout() );

      c.add( new JLabel( "Enter a string" ) );
      final JTextField input = new JTextField( 10 );
      c.add( input );       

      JButton addBtn = new JButton( "Add" );
      addBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               v.addElement( input.getText() );
               status.setText( "Added to end: " +
                               input.getText() );
               input.setText( "" ); 
            }
         }
      );         
      c.add( addBtn );      // add the input value

      JButton removeBtn = new JButton( "Remove" );
      removeBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               if ( v.removeElement( input.getText() ) )
                  status.setText( "Removed: " +
                                  input.getText() );
               else
                  status.setText( input.getText() +
                                  " not in vector" );
            }
         }
      );
      c.add( removeBtn );   

      JButton firstBtn = new JButton( "First" );
      firstBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               try {
                  status.setText( "First element: " +
                                  v.firstElement() );
               }
               catch ( NoSuchElementException exception ) {
                  status.setText( exception.toString() );
               }
            }
         }
      );
      c.add( firstBtn );    

      JButton lastBtn = new JButton( "Last" );
      lastBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               try {
                  status.setText( "Last element: " +
                                  v.lastElement() );
               }
               catch ( NoSuchElementException exception ) {
                  status.setText( exception.toString() );
               }
            }
         }
      );
      c.add( lastBtn );     

      JButton emptyBtn = new JButton( "Is Empty?" );
      emptyBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               status.setText( v.isEmpty() ?
                  "Vector is empty" : "Vector is not empty" );                                
            }
         }
      );
      c.add( emptyBtn );    

      JButton containsBtn = new JButton( "Contains" );
      containsBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               String searchKey = input.getText();

               if ( v.contains( searchKey ) ) 
                  status.setText( "Vector contains " +
                                  searchKey );
               else
                  status.setText( "Vector does not contain " +
                                  searchKey );
            }
         }
      );
      c.add( containsBtn ); 

      JButton locationBtn = new JButton( "Location" );
      locationBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               status.setText( "Element is at location " +
                               v.indexOf( input.getText() ) );
            }
         }
      );
      c.add( locationBtn );

      JButton trimBtn = new JButton( "Trim" );
      trimBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               v.trimToSize();
               status.setText( "Vector trimmed to size" );
            }
         }
      );
      c.add( trimBtn );     

      JButton statsBtn = new JButton( "Statistics" );
      statsBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               status.setText( "Size = " + v.size() +
                  "; capacity = " + v.capacity() );
            }
         }
      );
      c.add( statsBtn );    

      JButton displayBtn = new JButton( "Display" );
      displayBtn.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               Enumeration enum = v.elements();
               StringBuffer buf = new StringBuffer();
        
               while ( enum.hasMoreElements() )
                  buf.append(
                         enum.nextElement() ).append( "  " );
       
               JOptionPane.showMessageDialog( null,
                  buf.toString(), "Display",
                  JOptionPane.PLAIN_MESSAGE );
            }
         }
      );
      c.add( displayBtn );
      c.add( status );

      setSize( 300, 200 );
      show();
   }

   public static void main( String args[] )
   {
      VectorTest app = new VectorTest();

      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
