// Fig. 23.11: BitShift.java
// Using the bitwise shift operators.
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BitShift extends JFrame {

   public BitShift()
   {
      super( "Shifting bits" );

      Container c = getContentPane();
      c.setLayout( new FlowLayout() );
      final JTextField bits = new JTextField( 33 );
      c.add( new JLabel( "Integer to shift " ) );

      final JTextField value = new JTextField( 12 );
      value.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               int val = Integer.parseInt( value.getText() );
               bits.setText( getBits( val ) );
            }
         }
      );                   
      c.add( value );      
      
      bits.setEditable( false );
      c.add( bits );      

      JButton left = new JButton( "<<" );
      left.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               int val = Integer.parseInt( value.getText() );
               val <<= 1;
               value.setText( Integer.toString( val ) );
               bits.setText( getBits( val ) );
            }
         }
      );
      c.add( left );      

      JButton rightSign = new JButton( ">>" );
      rightSign.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               int val = Integer.parseInt( value.getText() );
               val >>= 1;
               value.setText( Integer.toString( val ) );
               bits.setText( getBits( val ) );
            }
         }
      );                    
      c.add( rightSign );      

      JButton rightZero = new JButton( ">>>" );
      rightZero.addActionListener(
         new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
               int val = Integer.parseInt( value.getText() );
               val >>>= 1;
               value.setText( Integer.toString( val ) );
               bits.setText( getBits( val ) );
            }
         }
      );
      c.add( rightZero );

      setSize( 400, 120 );
      show();
   }

   private String getBits( int value )
   {
      int displayMask = 1 << 31;
      StringBuffer buf = new StringBuffer( 35 );

      for ( int c = 1; c <= 32; c++ ) {
         buf.append(
            ( value & displayMask ) == 0 ? '0' : '1' );
         value <<= 1;

         if ( c % 8 == 0 )
            buf.append( ' ' );
      }

      return buf.toString();
   }

   public static void main( String args[] )
   {
      BitShift app = new BitShift();
      app.addWindowListener(
         new WindowAdapter() {
            public void windowClosing( WindowEvent e )
            {
               System.exit( 0 );
            }
         }
      );
   }
}


/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
