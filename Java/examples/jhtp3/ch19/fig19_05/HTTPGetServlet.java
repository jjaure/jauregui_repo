// Fig. 19.5: HTTPGetServlet.java
// Creating and sending a page to the client
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class HTTPGetServlet extends HttpServlet {
   public void doGet( HttpServletRequest request,
                      HttpServletResponse response )
      throws ServletException, IOException
   {
      PrintWriter output;

      response.setContentType( "text/html" );  // content type
      output = response.getWriter();           // get writer

      // create and send HTML page to client
      StringBuffer buf = new StringBuffer();
      buf.append( "<HTML><HEAD><TITLE>\n" );
      buf.append( "A Simple Servlet Example\n" );
      buf.append( "</TITLE></HEAD><BODY>\n" );
      buf.append( "<H1>Welcome to Servlets!</H1>\n" );
      buf.append( "</BODY></HTML>" );
      output.println( buf.toString() );
      output.close();    // close PrintWriter stream
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
