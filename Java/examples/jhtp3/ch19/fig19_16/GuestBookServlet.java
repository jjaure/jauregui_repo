// Fig. 19.16: GuestBookServlet.java
// Three-Tier Example
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.*;

public class GuestBookServlet extends HttpServlet {
   private Statement statement = null;
   private Connection connection = null;
   private String URL = "jdbc:odbc:GuestBook";

   public void init( ServletConfig config )
      throws ServletException
   {
      super.init( config );
	  
      try {
         Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
         connection = 
            DriverManager.getConnection( URL, "", "" );
      } 
      catch ( Exception e ) {
         e.printStackTrace();
         connection = null;
      }
   }

   public void doPost( HttpServletRequest req,
                       HttpServletResponse res )
      throws ServletException, IOException
   {
      String email, firstName, lastName, company,
             snailmailList, cppList, javaList, vbList,
             iwwwList;
			
      email = req.getParameter( "Email" );
      firstName = req.getParameter( "FirstName" );
      lastName = req.getParameter( "LastName" );
      company = req.getParameter( "Company" );
      snailmailList = req.getParameter( "mail" );
      cppList = req.getParameter( "c_cpp" );
      javaList = req.getParameter( "java" );
      vbList = req.getParameter( "vb" );
      iwwwList = req.getParameter( "iwww" );

      PrintWriter output = res.getWriter();
      res.setContentType( "text/html" );
	           
      if ( email.equals( "" ) ||
           firstName.equals( "" ) ||
           lastName.equals( "" ) ) {
         output.println( "<H3> Please click the back " +
                         "button and fill in all " +
                         "fields.</H3>" );
         output.close();
         return;
      }

      /* Note: The GuestBook database actually contains fields
       * Address1, Address2, City, State and Zip that are not
       * used in this example. However, the insert into the
       * database must still account for these fields. */
      boolean success = insertIntoDB(
         "'" + email + "','" + firstName + "','" + lastName +
         "','" + company + "',' ',' ',' ',' ',' ','" + 
         ( snailmailList != null ? "yes" : "no" ) + "','" +
         ( cppList != null ? "yes" : "no"  ) + "','" +
         ( javaList != null ? "yes" : "no"  ) + "','" +
         ( vbList != null ? "yes" : "no"  ) + "','" +
         ( iwwwList != null ? "yes" : "no"  ) + "'" );

      if ( success ) 
         output.print( "<H2>Thank you " + firstName +
                       " for registering.</H2>" );
      else
         output.print( "<H2>An error occurred. " +
                       "Please try again later.</H2>" );

      output.close();
   }

   private boolean insertIntoDB( String stringtoinsert )
   {
      try {
         statement = connection.createStatement();
         statement.execute( 
            "INSERT INTO GuestBook values (" +
            stringtoinsert + ");" );
         statement.close();
      }
      catch ( Exception e ) {
         System.err.println( 
            "ERROR: Problems with adding new entry" );
         e.printStackTrace();
         return false;
      }

      return true;
   }

   public void destroy()
   {
      try {
         connection.close();
      }
      catch( Exception e ) {
         System.err.println( "Problem closing the database" );
      }
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
