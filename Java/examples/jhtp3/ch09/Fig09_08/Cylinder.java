// Fig. 9.8: Cylinder.java
// Definition of class Cylinder
package com.deitel.jhtp3.ch09;

public class Cylinder extends Circle {
   protected double height;  // height of Cylinder

   // No-argument constructor 
   public Cylinder()
   {
      // implicit call to superclass constructor here
      setHeight( 0 );
   }

   // constructor
   public Cylinder( double h, double r, int a, int b )
   {
      super( r, a, b );
      setHeight( h );
   }

   // Set height of Cylinder
   public void setHeight( double h )  
      { height = ( h >= 0 ? h : 0 ); }

   // Get height of Cylinder
   public double getHeight() { return height; }

   // Calculate area of Cylinder (i.e., surface area)
   public double area() 
   {
      return 2 * super.area() + 
             2 * Math.PI * radius * height;
   }

   // Calculate volume of Cylinder
   public double volume() { return super.area() * height; }

   // Convert the Cylinder to a String
   public String toString()
   {
      return super.toString() + "; Height = " + height;
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
