// Fig. 9.6: Test.java
// Applet to test class Point
import javax.swing.JOptionPane;
import com.deitel.jhtp3.ch09.Point;

public class Test {
   public static void main( String args[] )
   {
      Point p = new Point( 72, 115 );
      String output;

      output = "X coordinate is " + p.getX() +
               "\nY coordinate is " + p.getY();

      p.setPoint( 10, 10 );

      // use implicit call to p.toString()
      output += "\n\nThe new location of p is " + p;

      JOptionPane.showMessageDialog( null, output,
         "Demonstrating Class Point",
         JOptionPane.INFORMATION_MESSAGE );
      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
