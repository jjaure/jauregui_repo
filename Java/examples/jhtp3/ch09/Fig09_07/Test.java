// Fig. 9.7: Test.java
// Applet to test class Circle
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
import com.deitel.jhtp3.ch09.Circle;

public class Test {
   public static void main( String args[] )
   {
      Circle c = new Circle( 2.5, 37, 43 );
      DecimalFormat precision2 = new DecimalFormat( "0.00" );
      String output;

      output = "X coordinate is " + c.getX() +
               "\nY coordinate is " + c.getY() +
               "\nRadius is " + c.getRadius();

      c.setRadius( 4.25 );
      c.setPoint( 2, 2 );
      output +=
         "\n\nThe new location and radius of c are\n" + c +
         "\nArea is " + precision2.format( c.area() );

      JOptionPane.showMessageDialog( null, output,
         "Demonstrating Class Circle",
         JOptionPane.INFORMATION_MESSAGE );
      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
