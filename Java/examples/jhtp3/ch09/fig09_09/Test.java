// Fig. 9.9: Test.java
// Driver for Employee hierarchy
import javax.swing.JOptionPane;
import java.text.DecimalFormat;

public class Test {
   public static void main( String args[] )
   {
      Employee ref;  // superclass reference
      String output = "";

      Boss b = new Boss( "John", "Smith", 800.00 );
      CommissionWorker c =
         new CommissionWorker( "Sue", "Jones",
                               400.0, 3.0, 150);
      PieceWorker p =
         new PieceWorker( "Bob", "Lewis", 2.5, 200 );
      HourlyWorker h =
         new HourlyWorker( "Karen", "Price", 13.75, 40 );

      DecimalFormat precision2 = new DecimalFormat( "0.00" );

      ref = b;  // Employee reference to a Boss 
      output += ref.toString() + " earned $" +
                precision2.format( ref.earnings() ) + "\n" +
                b.toString() + " earned $" +
                precision2.format( b.earnings() ) + "\n";
   
      ref = c;  // Employee reference to a CommissionWorker
      output += ref.toString() + " earned $" +
                precision2.format( ref.earnings() ) + "\n" +
                c.toString() + " earned $" +
                precision2.format( c.earnings() ) + "\n";
   
      ref = p;  // Employee reference to a PieceWorker
      output += ref.toString() + " earned $" +
                precision2.format( ref.earnings() ) + "\n" +
                p.toString() + " earned $" +
                precision2.format( p.earnings() ) + "\n";
   
      ref = h;  // Employee reference to an HourlyWorker
      output += ref.toString() + " earned $" +
                precision2.format( ref.earnings() ) + "\n" +
                h.toString() + " earned $" +
                precision2.format( h.earnings() ) + "\n";

      JOptionPane.showMessageDialog( null, output,
         "Demonstrating Polymorphism",
         JOptionPane.INFORMATION_MESSAGE );
      System.exit( 0 );
   }
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
