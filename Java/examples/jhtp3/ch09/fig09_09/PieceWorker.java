// Fig. 9.9: PieceWorker.java
// PieceWorker class derived from Employee

public final class PieceWorker extends Employee {
   private double wagePerPiece; // wage per piece output
   private int quantity;        // output for week

   // Constructor for class PieceWorker
   public PieceWorker( String first, String last,
                       double w, int q )
   {
      super( first, last );  // call superclass constructor
      setWage( w );
      setQuantity( q );
   }
   
   // Set the wage
   public void setWage( double w ) 
      { wagePerPiece = ( w > 0 ? w : 0 ); }
   
   // Set the number of items output
   public void setQuantity( int q ) 
      { quantity = ( q > 0 ? q : 0 ); }
   
   // Determine the PieceWorker's earnings
   public double earnings()
      { return quantity * wagePerPiece; }
   
   public String toString()
   {
      return "Piece worker: " + super.toString();
   }  
}

/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
