// Fig. 9.9: CommissionWorker.java
// CommissionWorker class derived from Employee

public final class CommissionWorker extends Employee {
   private double salary;      // base salary per week
   private double commission;  // amount per item sold
   private int quantity;       // total items sold for week

   // Constructor for class CommissionWorker
   public CommissionWorker( String first, String last,
                            double s, double c, int q)
   {
      super( first, last );  // call superclass constructor
      setSalary( s );
      setCommission( c );
      setQuantity( q );
   }
   
   // Set CommissionWorker's weekly base salary
   public void setSalary( double s )
      { salary = ( s > 0 ? s : 0 ); }
   
   // Set CommissionWorker's commission
   public void setCommission( double c ) 
      { commission = ( c > 0 ? c : 0 ); }
   
   // Set CommissionWorker's quantity sold
   public void setQuantity( int q )
      { quantity = ( q > 0 ? q : 0 ); }
   
   // Determine CommissionWorker's earnings
   public double earnings()
      { return salary + commission * quantity; }
   
   // Print the CommissionWorker's name 
   public String toString() 
   {
      return "Commission worker: " + super.toString();
   }
}   
   
/**************************************************************************
 * (C) Copyright 1999 by Deitel & Associates, Inc. and Prentice Hall.     *
 * All Rights Reserved.                                                   *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
