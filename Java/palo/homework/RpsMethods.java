package rpsmethods;
import java.util.Scanner; 
import java.util.Random;

public class RpsMethods {

    public static void main(String[] args) {
        Scanner takein = new Scanner(System.in);
        displayOpeningMessage();
        //int usermoves = 0 , commoves = 0;
        int usermain = 0 , commain = 0;
        while (usermain < 2 && commain < 2){
        System.out.print("Please select a weapon Scissor (0) Rock (1) Paper (2) :");
        int userchoice = getUserMove();
        int comchoice = getCPUMove();
        determineWinner (usermain, commain);
        //usermain = determineWinner (user);
        //commain = determineWinner (cpu);
        
        
        if (userchoice == 1 && comchoice == 1) {
            System.out.println("The Computer is rock. You are rock. The round is a draw"); 
        }
        if (userchoice == 0 && comchoice == 0) {
            System.out.println("The Computer is scissor. You are scissor. The round is a draw"); 
        if (userchoice == 2 && comchoice == 2) {
            System.out.println("The Computer is paper. You are paper. The round is a draw"); 
        }
        if (userchoice == 0 && comchoice == 1){
             System.out.println("The Computer is rock. You are scissor. You lost the round!");
             
        }
        if (userchoice == 0 && comchoice == 2){
             System.out.println("The Computer is paper. You are scissor. You won the round!");
             
        }
        if (userchoice == 1 && comchoice == 0){
             System.out.println("The Computer is scissor. You are rock. You won the round!");
             
        }
        if (userchoice == 1 && comchoice == 2){
           System.out.println("The Computer is paper. You are rock. You lost the round!");
           
        }
        if (userchoice == 2 && comchoice == 0 ){
             System.out.println("The Computer is scissor. You are paper. You lost the round!");
             
        }
        if (userchoice == 2 && comchoice == 1 ){
             System.out.println("The Computer is rock. You are paper. You won the round!");
             
        }
        }
        }  
    
    } 
    
    public static void displayOpeningMessage(){
      System.out.println("Rock, Paper, Scissor. Best out of three. Goodluck!");
      
    }
/**
* getUserMove will prompt the user for a move. A valid move is
* between 0 and 2. Any number less than 0 and bigger than 2 should
* be rejected. Any input that is not a number should also be rejected.
* This function should only return when a valid number is entered.
* @param in Scanner used for reading input from keyboard
* @return
*/
public static int getUserMove(){
    int getUserMove; 
   Scanner takein = new Scanner(System.in); 
   getUserMove = takein.nextInt(); 
    return getUserMove;
}
/**
* Generates a Random number between 0 and 2
* @return an integer between 0 and 2
*/
public static int getCPUMove(){
    int getCPUMove; 
    Random rnd = new Random(); 
    getCPUMove = rnd.nextInt(3); 
    return getCPUMove; 
}
/**
* Given a user and cpu move, this function determines who wins
* a game of Rock-Paper-Scissors.
* @param user user’s current move
* @param cpu computer’s current move
*/
public static void determineWinner(int user, int cpu){
    
    user = 0 ;
    cpu = 0 ;
    
        int userchoice = getUserMove();
        int comchoice = getCPUMove () ;
        
        
        if (userchoice == 0 && comchoice == 1){
             
             cpu++;
        }
        if (userchoice == 0 && comchoice == 2){
            
             user++;
        }
        if (userchoice == 1 && comchoice == 0){
             
             user++;
        }
        if (userchoice == 1 && comchoice == 2){
           
           cpu++;
        }
        if (userchoice == 2 && comchoice == 0 ){
             
             cpu++;
        }
        if (userchoice == 2 && comchoice == 1 ){
            
             user++; 
        }
    }
        
}
