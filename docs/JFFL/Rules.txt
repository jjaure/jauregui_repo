Rules And Regulations

A)  Admission
    Admission fee is $50 per team, due at the day of the draft.
    Cash only, no exceptions.  The Commish is responsible for 
    collecting these and any other fees occumulated throughout
    the season and displaying the league finances to every owner
    via a league wide posting.  At the end of the regular season,
    the Commish adds up all the funds and calculates winning
    payouts for the top 3 finishers.  

    Payouts will be as follows:

    1st Place - 70%
    2nd Place - 20%
    3rd Place - 10%

    Free Agency will only be allowed for teams qualifying for the
    playoffs.  The bottom four teams may continue managing their 
    teams and compete amongst themselves, but without Free Agency.

B)  The Draft
    The draft will be conducted "offline" at the Commish's 
    house.  Draft order will be determined by card draw, 
    with the Ace of spades representing the number one 
    overall pick.  Cards will be shuffled in front of all
    owners.
    The draft will be "snake style", with the first 
    round going 1-8, second round going 8-1, and so on, back
    and forth, for 18 rounds.  
    When the draft is over, each team will be given one free
    D&A, which is only valid for the week prior to the 
    beginning of the season.  This D&A will be first come, 
    first serve.  
C)  Free Agency
    Free Agency begins the Monday after the Sunday of each
    NFL week, and ends 8 p.m. on the Friday of the same week.  
    For weeks with Thursday games, free agency ends 24 hours
    before kickoff for players involved in that game only.
    For all other players, free agency ends on the normal 
    time.
    Each owner has the opportunity to update their rosters
    via free agency by declaring his interest in a player
    to the Commish.  It is the Commish's responsibility 
    to then inform the entire league and allow a 24 hour
    period of open bidding.  If no other owner shows 
    interest within the 24 hour period, the player is 
    awarded to the owner for $5 and the owner can update
    his roster once payment is received.  Otherwise, bidding
    can proceed, "Ebay Style", until a "higest bidder" is 
    left.  
    Free agency activity is communicated through text
    messaging.  It is assumed that each owner is OK with
    sending and receiving text messages.
    Any owner who does not pay for a D&A by the coming
    Sunday will not be allowed to participate in Free 
    Agency until his debt is paid.
D)  And Finally
    It is each owners' responsibility to read and uderstand
    these rules and regulations, and also the configuration
    of this league, such as scoring and roster configuration.
    Any questions whatsoever should be directed to the Commish
    through phone call, text message, e-mail, or league post.  
    This league is meant for people who love watching and 
    following football.  Any conflicts and/or conduct contrary 
    to the spirit of having fun and competing in a fair manner
    will be dealt with swiftly by the Commish.  The Commish settles
    all ties when voting, and his decision is final in all
    other matters.  
    With all that being said, this is OUR league, so please
    help me make it better by offering your input and 
    asking questions whenever anything doesn't make sense.

   Good Luck, and have fun!
